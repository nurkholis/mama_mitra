import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mama_mitra/pages/bottomNavPage.dart';
import 'package:mama_mitra/pages/homePage.dart';
import 'package:mama_mitra/pages/loginPage.dart';
import 'package:mama_mitra/pages/productFormPage.dart';
import 'package:mama_mitra/pages/productPage.dart';
import 'package:mama_mitra/pages/profilFormPage.dart';
import 'package:mama_mitra/pages/profilPage.dart';
import 'package:mama_mitra/pages/ubahPasswordPage.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Colors.white,
        // fontFamily: "NunitoSans-Regular",
      ),
      routes: {
        '/spalsh': (context) => SplashScreen(),
        '/login': (context) => LoginPage(),
        '/navigation': (context) => BottomNavPage(),
        '/product': (context) => ProductPage(),
        '/product/form': (context) => ProductFormPage(),
        '/profil': (context) => ProfilPage(),
        '/profil/form': (context) => ProfilFormPage(),
        '/ubah/password': (context) => UbahPasswordPage(),
      },
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 300.0,
          width: 300.0,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/illustration/login.png"),
                fit: BoxFit.cover),
          ),
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }

  void startTimer() {
    Timer(Duration(seconds: 1), () {
      navigateUser();
    });
  }

  void navigateUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var idToko = prefs.getString("id_toko");
    if (idToko == null) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginPage()));
    } else {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => BottomNavPage()));
    }
  }
}
