import 'package:flutter/material.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/pages/widget/formWidget.dart';
import 'package:mama_mitra/api/kategoriBarangApi.dart';
import 'package:mama_mitra/models/kategoriBarangModel.dart';

class a {
  static Future<KategoriBarangDataModel> showKategoribarangList(
      BuildContext context, String currentId) async {
    final TextEditingController nama_kategori_barang = TextEditingController();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    KategoriBarangDataModel result = new KategoriBarangDataModel();
    await showDialog(
      context: context,
      builder: (context) {
        String state = "list";
        bool is_send = false;
        String id_kategori_barang = "";
        String method = "POST";
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              title: state == "list"
                  ? SizedBox(
                      height: 20.0,
                      child: FlatButton(
                        onPressed: () {
                          setState(
                            () {
                              state = "form";
                              method = "POST";
                              nama_kategori_barang.text = null;
                            },
                          );
                        },
                        child: Text(
                          "Tambah",
                          style: Style.fontMediumPrimary,
                        ),
                      ),
                    )
                  : Container(),
              content: state == "list"
                  ? Container(
                      child: Wrap(
                        children: <Widget>[
                          Container(
                            height: 200.0,
                            child: FutureBuilder(
                              future: KategoriBarangApi()
                                  .getKategoriBarang(1, 1000, null),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (snapshot.data == null) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                } else {
                                  List<KategoriBarangDataModel> data =
                                      snapshot.data.data;
                                  if (data.length == 0) {
                                    return Center(
                                      child: Text(
                                        "Belum ada data",
                                        style: Style.fontMediumBold,
                                      ),
                                    );
                                  } else {
                                    return ListView.builder(
                                      itemCount: data.length,
                                      itemBuilder: (context, i) {
                                        return Column(
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 10.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  InkWell(
                                                    onTap: () {
                                                      setState(() {
                                                        result = data[i];
                                                      });
                                                      Navigator.pop(context);
                                                    },
                                                    child: Container(
                                                      child: Text(
                                                        data[i]
                                                            .nama_kategori_barang,
                                                        style: data[i]
                                                                    .id_kategori_barang ==
                                                                currentId
                                                            ? Style
                                                                .fontMediumPrimary
                                                            : Style.fontMedium,
                                                      ),
                                                    ),
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      setState(
                                                        () {
                                                          state = "form";
                                                          id_kategori_barang =
                                                              data[i]
                                                                  .id_kategori_barang;
                                                          nama_kategori_barang
                                                              .text = data[
                                                                  i]
                                                              .nama_kategori_barang;
                                                          method = "PUT";
                                                        },
                                                      );
                                                    },
                                                    child: Icon(
                                                      Icons.create,
                                                      size: 20.0,
                                                      color:
                                                          Pallet.primaryColor,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Divider(
                                              color: Colors.grey,
                                              height: 1.0,
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  }
                                }
                              },
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              RaisedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    side: BorderSide(color: Colors.grey)),
                                elevation: 0.0,
                                child: Text(
                                  "Batal",
                                  style: Style.fontMediumBold,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  : Container(
                      height: 150.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Form(
                            key: formKey,
                            child: SingleChildScrollView(
                              child: formWidget().textFormField(
                                  nama_kategori_barang,
                                  "Nama Kategori",
                                  TextInputType.text,
                                  30),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              RaisedButton(
                                onPressed: () {
                                  setState(() {
                                    state = "list";
                                  });
                                },
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    side: BorderSide(color: Colors.grey)),
                                elevation: 0.0,
                                child: Text(
                                  "Batal",
                                  style: Style.fontMediumBold,
                                ),
                              ),
                              RaisedButton(
                                onPressed: () {
                                  if (formKey.currentState.validate()) {
                                    setState(() {
                                      is_send = !is_send;
                                    });
                                    formKey.currentState.save();
                                    KategoriBarangDataModel data =
                                        KategoriBarangDataModel.fromJson({
                                      "id_kategori_barang": id_kategori_barang,
                                      "nama_kategori_barang":
                                          nama_kategori_barang.text,
                                    });
                                    KategoriBarangApi()
                                        .storeKategoriBarang(data, method)
                                        .then((r) {
                                      setState(() {
                                        is_send = !is_send;
                                        nama_kategori_barang.text = "";
                                        result = r;
                                      });
                                      Navigator.pop(context);
                                    }).catchError((e) {});
                                  }
                                },
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    side:
                                        BorderSide(color: Pallet.primaryColor)),
                                elevation: 0.0,
                                child: is_send == false
                                    ? Text(
                                        "Simpan",
                                        style: Style.fontMediumPrimary,
                                      )
                                    : SizedBox(
                                        height: 20.0,
                                        width: 20.0,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 3.0,
                                        ),
                                      ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
            );
          },
        );
      },
    );
    return result;
  }
}
