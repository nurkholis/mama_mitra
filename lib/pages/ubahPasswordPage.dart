import 'package:flutter/material.dart';
import 'package:mama_mitra/api/tokoApi.dart';
import 'package:mama_mitra/config/dialogs.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/pages/widget/formWidget.dart';

class UbahPasswordPage extends StatefulWidget {
  @override
  _UbahPasswordPageState createState() => _UbahPasswordPageState();
}

class _UbahPasswordPageState extends State<UbahPasswordPage> {
  GlobalKey<ScaffoldState> scafoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController passwordBaru = TextEditingController();
  TextEditingController passwordKonfirmasi = TextEditingController();
  bool isLoading = false;
  String method = "PUT";

  simpan() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      if (passwordBaru.text != passwordKonfirmasi.text) {
        Dialogs.showAlertSnackbar(
            context, scafoldKey, "Gagal", "isian ulangi password tidak sama");
        return;
      }
      setState(() {
        isLoading = true;
      });
      TokoApi().gantiPassword(passwordBaru.text).then((r) {
        setState(() {
          isLoading = false;
        });
        if (r == "success") {
          Navigator.of(context).pop();
        } else {
          Dialogs.showAlertSnackbar(context, scafoldKey, "Gagal", r);
        }
      }).catchError((e) {
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scafoldKey,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        title: Text(
          "Ubah Pasword",
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                formWidget().textFormField(
                    passwordBaru, "Password Baru", TextInputType.text, 30),
                formWidget().textFormField(passwordKonfirmasi,
                    "Ulangi Password", TextInputType.text, 30),
                SizedBox(
                  width: double.infinity,
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      simpan();
                    },
                    elevation: 1.0,
                    color: Pallet.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: isLoading
                        ? SizedBox(
                            width: 16.0,
                            height: 16.0,
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                              strokeWidth: 2.0,
                            ),
                          )
                        : Text(
                            "Simpan",
                            style: Style.fontMediumWhite,
                          ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
