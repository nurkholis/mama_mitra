import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mama_mitra/api/tokoApi.dart';
import 'package:mama_mitra/config/dialogs.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:mama_mitra/config/imagePickCrop.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/models/tokoModel.dart';
import 'package:mama_mitra/pages/widget/formWidget.dart';

class ProfilFormPage extends StatefulWidget {
  TokoDataModel toko = new TokoDataModel();
  ProfilFormPage({
    this.toko,
  });
  @override
  _ProfilFormPageState createState() => _ProfilFormPageState();
}

class _ProfilFormPageState extends State<ProfilFormPage> {
  GlobalKey<ScaffoldState> scafoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController namaToko = TextEditingController();
  TextEditingController teleponToko = TextEditingController();
  TextEditingController email = TextEditingController();
  String tempLogoToko;
  File imageFile;
  bool isLoading = false;
  String method = "PUT";

  @override
  void initState() {
    namaToko.text = widget.toko.nama_toko;
    teleponToko.text = widget.toko.telepon_toko;
    email.text = widget.toko.email;
    tempLogoToko = widget.toko.logo_toko;
    super.initState();
  }

  simpan() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      setState(() {
        isLoading = true;
      });

      TokoDataModel data = TokoDataModel.fromJson(
        {
          "id_toko": widget.toko != null ? widget.toko.id_toko : "",
          "nama_toko": namaToko.text,
          "telepon_toko": teleponToko.text,
          "email": email.text,
          "logo_toko": imageFile != null
              ? 'data:image/jpeg;base64,' +
                  base64Encode(imageFile.readAsBytesSync())
              : "",
        },
      );
      TokoApi().storeToko(data, method).then((r) {
        setState(() {
          isLoading = false;
        });
        if (r == "success") {
          Navigator.of(context).pop();
        } else {
          Dialogs.showAlertSnackbar(context, scafoldKey, "Gagal", r);
        }
      }).catchError((e) {
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scafoldKey,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        title: Text(
          "Ubah Profil",
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                buildInputLogoToko(context),
                formWidget()
                    .textFormField(namaToko, "Nama", TextInputType.text, 30),
                formWidget().textFormField(
                    teleponToko, "Telepon", TextInputType.phone, 15),
                formWidget().textFormField(
                    email, "Telepon", TextInputType.emailAddress, 50),
                SizedBox(
                  width: double.infinity,
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      simpan();
                    },
                    elevation: 1.0,
                    color: Pallet.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: isLoading
                        ? SizedBox(
                            width: 16.0,
                            height: 16.0,
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                              strokeWidth: 2.0,
                            ),
                          )
                        : Text(
                            "Simpan",
                            style: Style.fontMediumWhite,
                          ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container buildInputLogoToko(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Stack(
            fit: StackFit.loose,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 120.0,
                    height: 120.0,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(color: Colors.grey.shade500),
                    ),
                    child: _imageState(),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 90.0, left: 100.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        ImagePickCrop.showChoiceDialog(context).then((r) {
                          setState(() {
                            imageFile = r;
                          });
                        });
                      },
                      shape: CircleBorder(),
                      color: Pallet.primaryColor,
                      child: Icon(
                        Icons.camera_alt,
                        size: 15.0,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  ClipRRect _imageState() {
    if (imageFile != null) {
      return ClipRRect(
        child: Image.file(
          imageFile,
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(5.0),
      );
    } else if (tempLogoToko != null) {
      return ClipRRect(
        child: Image.network(
          EndPoint.host + "/images/logo/" + tempLogoToko,
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(5.0),
      );
    } else {
      return ClipRRect(
        child: Image.asset(
          "assets/images/illustration/preload.png",
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(5.0),
      );
    }
  }
}
