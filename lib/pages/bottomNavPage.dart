import 'package:flutter/material.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/pages/historyPage.dart';
import 'package:mama_mitra/pages/homePage.dart';
import 'package:mama_mitra/pages/productPage.dart';
import 'package:mama_mitra/pages/profilPage.dart';

class BottomNavPage extends StatefulWidget {
  @override
  _BottomNavPageState createState() => _BottomNavPageState();
}

class _BottomNavPageState extends State<BottomNavPage> {
  final List _scren = [
    HomePage(),
    ProductPage(),
    HistoryPage(),
    ProfilPage(),
  ];
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _scren[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        backgroundColor: Colors.white,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        elevation: 30.0,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          Icons.home,
          Icons.restaurant_menu,
          Icons.event_note,
          Icons.person
        ]
            .asMap()
            .map(
              (key, value) => MapEntry(
                key,
                BottomNavigationBarItem(
                  title: Text(""),
                  icon: Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 6.0, horizontal: 16.0),
                    decoration: BoxDecoration(
                      color: _currentIndex == key
                          ? Pallet.primaryColor
                          : Colors.transparent,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Icon(value),
                  ),
                ),
              ),
            )
            .values
            .toList(),
      ),
    );
  }
}
