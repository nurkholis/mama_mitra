import 'package:flutter/material.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/models/penjualanModel.dart';
import 'package:intl/intl.dart';

class HistoryDetailPage extends StatefulWidget {
  final PenjualanDataModel penjualan;
  HistoryDetailPage({this.penjualan});
  @override
  _HistoryDetailPageState createState() => _HistoryDetailPageState();
}

class _HistoryDetailPageState extends State<HistoryDetailPage> {
  PenjualanDataModel data;
  @override
  void initState() {
    data = widget.penjualan;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        title: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                data.kode_penjualan,
                style: Style.fontMediumBlackBold,
              ),
              Text(
                data.status_penjualan == "0"
                    ? "Menunggu"
                    : data.status_penjualan == "1" ? "Proses" : "Selesai",
                style: Style.fontSmall,
              ),
            ],
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Center(
              child: Text(
                data.tanggal,
                style: Style.fontSmallBold,
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              data.driver != null ? buildDriverDetail() : Container(),
              buildDeliveryDetailWidget(),
              buildPaymentDetailWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildDriverDetail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("Detail Driver", style: Style.fontMediumBlackBold),
        ListTile(
          contentPadding: EdgeInsets.zero,
          leading: Container(
            width: 60.0,
            height: 60.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: ClipRRect(
              child: Image.network(
                EndPoint.host + "/images/driver/" + data.driver.foto_driver,
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(50.0),
            ),
          ),
          title: Text(
            data.driver.nama_driver.toUpperCase(),
            style: Style.fontMediumBold,
          ),
          subtitle: Text(
            data.driver.telepon_driver,
            style: Style.fontSmall,
          ),
        ),
        Divider(
          color: Colors.grey,
          height: 20.0,
        ),
      ],
    );
  }

  Widget buildDeliveryDetailWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("Detail Pengantaran", style: Style.fontMediumBlackBold),
        ListTile(
          contentPadding: EdgeInsets.only(top: 10.0),
          leading: Container(
            width: 30.0,
            height: 30.0,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: Icon(
              Icons.restaurant,
              color: Colors.white,
              size: 16.0,
            ),
          ),
          title: Padding(
            padding: const EdgeInsets.only(bottom: 5.0),
            child: Text(
              "Alamat Restoran",
              style: Style.fontSmall,
            ),
          ),
          subtitle: Text(
            data.toko.alamat_lengkap_toko.toUpperCase(),
            style: Style.fontMediumBold,
          ),
        ),
        ListTile(
          contentPadding: EdgeInsets.only(top: 10.0),
          leading: Container(
            width: 30.0,
            height: 30.0,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: Icon(
              Icons.person_pin,
              color: Colors.white,
              size: 16.0,
            ),
          ),
          title: Padding(
            padding: const EdgeInsets.only(bottom: 5.0),
            child: Text(
              "Alamat Tujuan",
              style: Style.fontSmall,
            ),
          ),
          subtitle: Text(
            data.alamat_lengkap_penjualan.toUpperCase(),
            style: Style.fontMediumBold,
          ),
        ),
        Divider(
          color: Colors.grey,
          height: 20.0,
        )
      ],
    );
  }

  Widget buildPaymentDetailWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("Detail Pembayaran", style: Style.fontMediumBlackBold),
        Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                for (var i = 0; i < data.penjualanItem.data.length; i++)
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          data.penjualanItem.data[i].barang.nama_barang,
                          style: Style.fontSmallBold,
                        ),
                        Text(
                          NumberFormat.simpleCurrency(
                                  locale: "IDR", decimalDigits: 0)
                              .format(double.parse(data
                                  .penjualanItem.data[i].barang.harga_barang)),
                          style: Style.fontSmallBold,
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Ongkir",
                style: Style.fontSmallBold,
              ),
              Text(
                NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0)
                    .format(double.parse(data.ongkir)),
                style: Style.fontSmallBold,
              ),
            ],
          ),
        ),
        Divider(
          color: Colors.grey,
          height: 1.0,
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Total",
                style: Style.fontMediumBlackBold,
              ),
              Text(
                NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0)
                    .format(double.parse(data.total_penjualan)),
                style: Style.fontMediumBold,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
