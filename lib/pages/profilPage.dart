import 'package:flutter/material.dart';
import 'package:mama_mitra/api/tokoApi.dart';
import 'package:mama_mitra/config/dialogs.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/main.dart';
import 'package:mama_mitra/models/tokoModel.dart';
import 'package:mama_mitra/pages/loginPage.dart';
import 'package:mama_mitra/pages/profilFormPage.dart';
import 'package:mama_mitra/pages/skaleton/listSkaleton.dart';
import 'package:mama_mitra/config/string_extension.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilPage extends StatefulWidget {
  @override
  _ProfilPageState createState() => _ProfilPageState();
}

class _ProfilPageState extends State<ProfilPage> {
  TokoDataModel toko = TokoDataModel();
  GlobalKey<ScaffoldState> scafoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scafoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Profi Restoran",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: FutureBuilder(
        future: TokoApi().getTokoku(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return ListSkaleton();
          } else {
            toko = snapshot.data;
            return SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ListTile(
                      contentPadding: EdgeInsets.symmetric(vertical: 5.0),
                      leading: Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.all(
                            Radius.circular(50.0),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            toko.nama_toko[0].toUpperCase(),
                            style: Style.fontMediumWhite,
                          ),
                        ),
                      ),
                      title: Padding(
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: Text(
                          toko.nama_toko.capitalize(),
                          style: Style.fontLargeBlackBold,
                        ),
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            toko.telepon_toko,
                            style: Style.fontMedium,
                          ),
                          Text(
                            toko.email,
                            style: Style.fontMedium,
                          ),
                        ],
                      ),
                      trailing: InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ProfilFormPage(
                                    toko: toko,
                                  )));
                        },
                        child: Icon(
                          Icons.edit,
                          color: Pallet.primaryColor,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 40.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Info Lainnya",
                            style: Style.fontMediumBlackBold,
                          ),
                          buildConfig("Bantuan", Icons.help_outline, () {}),
                          buildConfig(
                              "Beri Kami Rating", Icons.star_border, () {}),
                          buildConfig(
                              "Kebijakan Privasi", Icons.verified_user, () {}),
                          buildConfig("Ketentuan Pengguna", Icons.gavel, () {}),
                          buildConfig("Ganti Password", Icons.lock, () {
                            gantiPassword();
                          }),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 20.0),
                            child: SizedBox(
                              height: 50.0,
                              width: double.infinity,
                              child: RaisedButton(
                                elevation: 0.0,
                                onPressed: () {
                                  logout();
                                },
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(5.0),
                                    ),
                                    side:
                                        BorderSide(color: Pallet.primaryColor)),
                                child: Text(
                                  "Keluar",
                                  style: Style.fontMediumPrimary,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  void gantiPassword() {
    // Navigator.of(context).push(MaterialPageRoute(builder: (context)=> MemberPasswordFormPage());
  }

  void logout() async {
    Dialogs.showConfirmDialog(
        context, scafoldKey, "Apakah kamu yakin ingin keluar?", () async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LoginPage()),
          (Route<dynamic> route) => false);

      prefs.clear();
    });
  }

  Column buildConfig(String name, IconData icon, f) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: f,
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 2.0),
            leading: Icon(
              icon,
              size: 25.0,
            ),
            title: Text(
              name,
              style: Style.fontMedium,
            ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 16.0,
            ),
          ),
        ),
        Divider(
          height: 1.0,
          color: Colors.grey,
        ),
      ],
    );
  }
}
