import 'package:flutter/material.dart';
import 'package:mama_mitra/config/dialogs.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/api/tokoApi.dart';

import 'bottomNavPage.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isSend = false;

  final GlobalKey<ScaffoldState> scafoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  var raisedDecoration = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(10.0),
    boxShadow: [
      BoxShadow(
        color: Colors.black26,
        offset: Offset(2, 2),
        blurRadius: 2.0,
        spreadRadius: 0.0,
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    _login() {
      if (formKey.currentState.validate()) {
        formKey.currentState.save();
        setState(() {
          _isSend = true;
        });
        print(email.text);
        print(password.text);
        TokoApi().loginToko(email.text, password.text, "#").then((r) {
          setState(() {
            _isSend = false;
          });
          if (r) {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => BottomNavPage()));
          } else {
            Dialogs.showAlertSnackbar(
                context, scafoldKey, "Gagal", "email atau password salah");
          }
        }).catchError((e) {
          Dialogs.showAlertSnackbar(
              context, scafoldKey, "Gagal", "email atau password salah");
        });
      }
    }

    return Scaffold(
      key: scafoldKey,
      backgroundColor: Pallet.primaryColor,
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 30.0,
          ),
          _topheader(),
          Expanded(
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.only(top: 30),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
                color: Colors.grey[50],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: SingleChildScrollView(
                child: Form(
                  key: formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 20),
                      _labelText("Email"),
                      _inputTextField(
                          email, "Email", "misal: mama@gmail.com", false),
                      SizedBox(height: 16),
                      _labelText("Password"),
                      _inputTextField(password, "Password", "********", true),
                      SizedBox(height: 12),
                      Align(
                        alignment: Alignment.centerRight,
                        child: InkWell(
                          onTap: () {},
                          child: Text(
                            "Lupa password?",
                            style: TextStyle(
                              color: Colors.blue[900],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          height: 46,
                          width: 200.0,
                          child: RaisedButton(
                            onPressed: () {
                              _login();
                              // SnackBar(content: Text("hai"));
                              // _showSnackBar();
                            },
                            child: _isSend
                                ? SizedBox(
                                    height: 20.0,
                                    width: 20.0,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 3,
                                    ),
                                  )
                                : Text(
                                    'Login',
                                    style: Style.fontMediumWhite,
                                  ),
                            color: Pallet.primaryColor,
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _inputTextField(TextEditingController controller, String label,
      String hintText, bool obscuretext) {
    return Container(
      height: 56,
      padding: EdgeInsets.fromLTRB(16, 3, 16, 6),
      margin: EdgeInsets.all(8),
      decoration: raisedDecoration,
      child: Center(
        child: TextFormField(
          controller: controller,
          keyboardType: TextInputType.emailAddress,
          obscureText: obscuretext,
          decoration: InputDecoration(
            hintText: hintText,
            border: InputBorder.none,
            hintStyle: TextStyle(
              color: Colors.black38,
            ),
          ),
          validator: (String value) {
            if (value.isEmpty) {
              return label + ' Harus Diisi';
            }
            return null;
          },
        ),
      ),
    );
  }

  _labelText(String title) {
    return Padding(
      padding: EdgeInsets.only(left: 24),
      child: Text(
        title,
        style: Style.fontMediumBlackBold,
      ),
    );
  }

  _topheader() {
    return Padding(
      padding: EdgeInsets.only(left: 32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Login\nMama\nMitra',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w600,
              fontSize: 30.0,
            ),
          ),
          Image.asset(
            'assets/images/illustration/login.png',
            height: 200.0,
            fit: BoxFit.fitHeight,
          )
        ],
      ),
    );
  }
}
