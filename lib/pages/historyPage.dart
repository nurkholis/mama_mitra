import 'package:flutter/material.dart';
import 'package:mama_mitra/api/penjualanApi.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/models/penjualanModel.dart';
import 'package:mama_mitra/pages/historyDetailPage.dart';
import 'package:mama_mitra/pages/skaleton/listWithImageSkaleton.dart';
import 'package:mama_mitra/pages/widget/emptyPageWidget.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  Future<PenjualanModel> firstData;
  PenjualanModel penjualanModel;
  int currentPage = 1;
  int totalPage = 1;
  bool isLoadNext = false;
  ScrollController scrollController = new ScrollController();
  List<PenjualanDataModel> data;
  @override
  void initState() {
    firstData = PenjualanApi().getPenjualan(1, 10, "");
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Riwayat Pesanan",
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: FutureBuilder(
        future: firstData,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return ListWithImageSkaleton();
          } else {
            if (snapshot.data == null) {
              return EmptyPageWidget().noInternetConnection(() {
                setState(() {
                  firstData = PenjualanApi().getPenjualan(1, 10, "");
                  currentPage = 1;
                });
              });
            } else {
              penjualanModel = snapshot.data;
              data = penjualanModel.data;
              totalPage = penjualanModel.pagination.total_pages;
              if (data.length > 0) {
                return buildListPenjualan();
              } else {
                return EmptyPageWidget().noData(() {
                  setState(() {
                    firstData = PenjualanApi().getPenjualan(1, 10, "");
                    currentPage = 1;
                  });
                });
              }
            }
          }
        },
      ),
    );
  }

  Widget buildListPenjualan() {
    return NotificationListener(
      child: ListView.builder(
        controller: scrollController,
        itemCount: data.length,
        itemBuilder: (context, i) {
          if (isLoadNext && i == data.length - 1) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => HistoryDetailPage(
                            penjualan: data[i],
                          ),
                        ),
                      );
                    },
                    child: Container(
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(vertical: 10.0),
                        leading: Container(
                          width: 60.0,
                          height: 60.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: ClipRRect(
                            child: FadeInImage.assetNetwork(
                              placeholder:
                                  "assets/images/illustration/preload.png",
                              image: EndPoint.host +
                                  "/images/barang/" +
                                  data[0]
                                      .penjualanItem
                                      .data[0]
                                      .barang
                                      .foto_barang,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                        title: Text(
                          data[0].penjualanItem.data.length == 1
                              ? data[0].penjualanItem.data[0].barang.nama_barang
                              : data[0]
                                      .penjualanItem
                                      .data[0]
                                      .barang
                                      .nama_barang +
                                  " dan " +
                                  (data[0].penjualanItem.data.length - 1)
                                      .toString() +
                                  " lainnya",
                          style: Style.fontMediumBlackBold,
                        ),
                        subtitle: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                data[i].status_penjualan == "0"
                                    ? "Menunggu"
                                    : data[i].status_penjualan == "1"
                                        ? "Proses"
                                        : "Selesai",
                                style: Style.fontSmall,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Text(
                                  "${data[i].kode_penjualan}, ${data[i].tanggal}",
                                  style: Style.fontSmall,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 1.0,
                  )
                ],
              ),
            );
          }
        },
      ),
      onNotification: (notification) {
        if (notification is ScrollUpdateNotification) {
          if (scrollController.offset >=
                  scrollController.position.maxScrollExtent &&
              !scrollController.position.outOfRange) {
            if (currentPage < totalPage) {
              setState(() {
                isLoadNext = true;
              });

              PenjualanApi().getPenjualan(currentPage + 1, 10, "").then((r) {
                setState(() {
                  currentPage = r.pagination.current_page;
                  totalPage = r.pagination.total_pages;
                  data.addAll(r.data);
                  setState(() {
                    isLoadNext = false;
                  });
                });
              });
            }
          }
        }
        return true;
      },
    );
  }
}
