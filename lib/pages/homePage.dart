import 'package:flutter/material.dart';
import 'package:mama_mitra/pages/widget/grafikBulananWidget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Beranda",
          style: TextStyle(
            color: Colors.black87,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            children: <Widget>[
              Text("Grafik Penjualan Bualanan"),
              GrafikBulananWidget()
            ],
          ),
        ),
      ),
    );
  }
}
