import 'dart:io';
import 'package:flutter/material.dart';
import 'package:mama_mitra/api/barangApi.dart';
import 'package:mama_mitra/config/dialogs.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/models/barangModel.dart';
import 'package:mama_mitra/models/kategoriBarangModel.dart';
import 'package:mama_mitra/pages/widget/formWidget.dart';
import 'package:mama_mitra/pages/select/productCategorySelect.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:mama_mitra/config/imagePickCrop.dart';

class ProductFormPage extends StatefulWidget {
  final BarangDataModel barang;
  ProductFormPage({this.barang});
  @override
  _ProductFormPageState createState() => _ProductFormPageState();
}

class _ProductFormPageState extends State<ProductFormPage> {
  GlobalKey<ScaffoldState> _scafoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _nama_barang = TextEditingController();
  TextEditingController _harga_barang = TextEditingController();
  TextEditingController _harga_promo = TextEditingController();
  TextEditingController _nama_kategori_barang = TextEditingController();
  KategoriBarangDataModel _kategoriBarang = KategoriBarangDataModel();
  TextEditingController _deskripsi_barang = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _temp_foto_barang;
  bool _terlihat = true;
  File _imageFile;
  bool _isLoading = false;
  String _method = "POST";

  void initState() {
    if (widget.barang != null) {
      _method = "PUT";
      _nama_barang.text = widget.barang.nama_barang;
      _harga_barang.text =
          NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0)
              .format(double.parse(widget.barang.harga_barang));
      _harga_promo.text = widget.barang.harga_promo == "null"
          ? null
          : NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0)
              .format(double.parse(widget.barang.harga_promo));
      _deskripsi_barang.text = widget.barang.deskripsi_barang;
      _nama_kategori_barang.text =
          widget.barang.kategori_barang.nama_kategori_barang;
      _kategoriBarang.id_kategori_barang =
          widget.barang.kategori_barang.id_kategori_barang;
      _terlihat = widget.barang.terlihat == "1" ? true : false;
      _temp_foto_barang = widget.barang.foto_barang;
    }
    super.initState();
  }

  _simpan() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      if (_harga_promo.text.isNotEmpty) {
        if (double.parse(_harga_barang.text.replaceAll(RegExp('[^0-9]'), "")) <
            double.parse(_harga_promo.text.replaceAll(RegExp('[^0-9]'), ""))) {
          Dialogs.showAlertSnackbar(context, _scafoldKey, "Gagal",
              "harga promo harus lebih kecil dari harga barang");
          return;
        }
      }
      BarangDataModel data = BarangDataModel.fromJson(
        {
          "id_barang": widget.barang != null ? widget.barang.id_barang : "",
          "id_kategori_barang": _kategoriBarang.id_kategori_barang,
          "nama_barang": _nama_barang.text,
          "harga_barang": _harga_barang.text.replaceAll(RegExp('[^0-9]'), ""),
          "harga_promo": _harga_promo.text.isNotEmpty
              ? _harga_promo.text.replaceAll(RegExp('[^0-9]'), "")
              : null,
          "berat_barat": '50',
          "stok_barang": '100',
          "deskripsi_barang": _deskripsi_barang.text,
          "terlihat": _terlihat == true ? "1" : "0",
          "foto_barang": _imageFile != null
              ? 'data:image/jpeg;base64,' +
                  base64Encode(_imageFile.readAsBytesSync())
              : 0,
        },
      );

      setState(() {
        _isLoading = true;
      });
      BarangApi().storeBarang(data, _method).then((r) {
        setState(() {
          _isLoading = false;
        });
        if (r == "success") {
          Navigator.pop(context, "success");
        } else {
          Dialogs.showAlertSnackbar(context, _scafoldKey, "Gagal", r);
        }
      }).catchError((e) {
        setState(() {
          _isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scafoldKey,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Text(
          "Form Menu",
          style: TextStyle(color: Colors.black87),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              buildInputProductImage(context),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    formWidget().textFormField(
                        _nama_barang, "Nama Menu", TextInputType.text, 50),
                    formWidget().textFormFieldClickable(
                      _nama_kategori_barang,
                      "Kategori Menu",
                      TextInputType.text,
                      20,
                      Icons.arrow_forward_ios,
                      () {
                        a
                            .showKategoribarangList(
                                context, _kategoriBarang.id_kategori_barang)
                            .then((KategoriBarangDataModel r) {
                          _kategoriBarang = r;
                          _nama_kategori_barang.text =
                              _kategoriBarang.nama_kategori_barang;
                        });
                      },
                    ),
                    formWidget().textFormFieldIDR(
                        _harga_barang, "Harga Menu", TextInputType.number, 9),
                    formWidget().textFormAreaField(_deskripsi_barang,
                        "Deskripsi Menu", TextInputType.multiline, 300),
                  ],
                ),
              ),
              Text(
                "* jangan diisi jika tidak ada promo",
                style: Style.fontSmall,
              ),
              formWidget().textFormFieldIDR(
                  _harga_promo, "Harga Promo", TextInputType.number, 9),
              Container(
                padding: EdgeInsets.symmetric(vertical: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Terlihat",
                      style: Style.fontMediumBold,
                    ),
                    Switch(
                      value: _terlihat,
                      onChanged: (bool val) {
                        setState(() {
                          _terlihat = !_terlihat;
                        });
                      },
                      activeColor: Pallet.primaryColor,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: double.infinity,
                height: 50.0,
                child: RaisedButton(
                  onPressed: () {
                    _simpan();
                  },
                  elevation: 1.0,
                  color: Pallet.primaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: _isLoading
                      ? SizedBox(
                          width: 16.0,
                          height: 16.0,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                            strokeWidth: 2.0,
                          ),
                        )
                      : Text(
                          "Simpan",
                          style: Style.fontMediumWhite,
                        ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container buildInputProductImage(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Stack(
            fit: StackFit.loose,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 120.0,
                    height: 120.0,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(color: Colors.grey.shade500),
                    ),
                    child: _imageState(),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 90.0, left: 100.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        ImagePickCrop.showChoiceDialog(context).then((r) {
                          setState(() {
                            _imageFile = r;
                          });
                        });
                      },
                      shape: CircleBorder(),
                      color: Pallet.primaryColor,
                      child: Icon(
                        Icons.camera_alt,
                        size: 15.0,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  ClipRRect _imageState() {
    if (_imageFile != null) {
      return ClipRRect(
        child: Image.file(
          _imageFile,
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(5.0),
      );
    } else if (_temp_foto_barang != null) {
      return ClipRRect(
        child: Image.network(
          EndPoint.host + "/images/barang/" + _temp_foto_barang,
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(5.0),
      );
    } else {
      return ClipRRect(
        child: Image.asset(
          "assets/images/illustration/preload.png",
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(5.0),
      );
    }
  }
}
