import 'package:flutter/material.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:mama_mitra/models/barangModel.dart';
import 'package:mama_mitra/pages/productFormPage.dart';
import 'package:mama_mitra/pages/skaleton/listWithImageSkaleton.dart';
import 'package:mama_mitra/api/BarangApi.dart';
import 'package:mama_mitra/pages/widget/emptyPageWidget.dart';
import 'package:intl/intl.dart';

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  Future<BarangModel> firstData;
  ScrollController scrollController = new ScrollController();
  List<BarangDataModel> data;
  BarangModel barangModel;
  bool isLoadingNext = false;
  int currentPage = 1;
  int totalPage;
  @override
  void initState() {
    firstData = BarangApi().getBarang(1, 10, "");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Menu",
          style: TextStyle(color: Colors.black87),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            child: RaisedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ProductFormPage(),
                  ),
                );
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
                side: BorderSide(color: Pallet.primaryColor),
              ),
              color: Colors.white,
              elevation: 0.0,
              child: Text(
                "Tambah",
                style: Style.fontSmallPrimary,
              ),
            ),
          ),
        ],
      ),
      body: FutureBuilder(
        future: firstData,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return ListWithImageSkaleton();
          } else {
            if (snapshot.data == null) {
              return EmptyPageWidget().noInternetConnection(() {
                setState(() {
                  firstData = BarangApi().getBarang(1, 10, "");
                  currentPage = 1;
                });
              });
            } else {
              barangModel = snapshot.data;
              data = barangModel.data;
              totalPage = barangModel.pagination.total_pages;
              if (data.length > 0) {
                return buildListProduct();
              } else {
                return EmptyPageWidget().noData(() {
                  setState(() {
                    firstData = BarangApi().getBarang(1, 10, "");
                    currentPage = 1;
                  });
                });
              }
            }
          }
        },
      ),
    );
  }

  Widget buildListProduct() {
    return NotificationListener(
      child: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            firstData = BarangApi().getBarang(1, 10, "");
            currentPage = 1;
          });
        },
        child: ListView.builder(
          controller: scrollController,
          itemCount: data.length,
          itemBuilder: (context, i) {
            if (isLoadingNext && i == data.length - 1) {
              return Container(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          onTap: () async {
                            String f;
                            f = await Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => ProductFormPage(
                                  barang: data[i],
                                ),
                              ),
                            );
                            if (f == "success") {
                              setState(() {
                                firstData = BarangApi().getBarang(1, 10, "");
                                currentPage = 1;
                              });
                            }
                          },
                          child: ListTile(
                            contentPadding: EdgeInsets.symmetric(vertical: 5.0),
                            leading: Container(
                              width: 60.0,
                              height: 60.0,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: ClipRRect(
                                child: FadeInImage.assetNetwork(
                                  placeholder:
                                      "assets/images/illustration/preload.png",
                                  image: EndPoint.host +
                                      "/images/barang/" +
                                      data[i].foto_barang,
                                ),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                            ),
                            title: Text(
                              data[i].nama_barang,
                              style: Style.fontMediumBlackBold,
                            ),
                            subtitle: data[i].harga_promo == "null"
                                ? Text(
                                    "${NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0).format(double.parse(data[i].harga_barang))}")
                                : Row(
                                    children: <Widget>[
                                      Text(
                                        "${NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0).format(double.parse(data[i].harga_barang))}",
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.lineThrough,
                                            color: Colors.red),
                                      ),
                                      Text(
                                          " ${NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0).format(double.parse(data[i].harga_promo))}")
                                    ],
                                  ),
                          ),
                        ),
                        Divider(
                          color: Colors.grey,
                          height: 1.0,
                        )
                      ],
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
      onNotification: (notification) {
        if (notification is ScrollUpdateNotification) {
          if (scrollController.offset >=
                  scrollController.position.maxScrollExtent &&
              !scrollController.position.outOfRange) {
            if (currentPage < totalPage) {
              setState(() {
                isLoadingNext = true;
              });
              BarangApi().getBarang(currentPage + 1, 10, "").then((r) {
                setState(() {
                  currentPage = r.pagination.current_page;
                  totalPage = r.pagination.total_pages;
                  data.addAll(r.data);
                  isLoadingNext = false;
                });
              });
            }
          }
        }
        return true;
      },
    );
  }
}
