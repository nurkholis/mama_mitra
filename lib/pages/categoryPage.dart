import 'package:flutter/material.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/models/kategoriBarangModel.dart';
import 'package:mama_mitra/pages/skaleton/listSkaleton.dart';
import 'package:mama_mitra/api/kategoriBarangApi.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Kategori Menu",
          style: TextStyle(color: Colors.black87),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Container(
            padding: EdgeInsets.all(10.0),
            child: RaisedButton(
              onPressed: () {
                addCategory(context, _scaffoldKey);
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
                side: BorderSide(color: Pallet.primaryColor),
              ),
              color: Colors.white,
              elevation: 0.0,
              child: Text(
                "Tambah",
                style: Style.fontSmallPrimary,
              ),
            ),
          ),
        ],
      ),
      body: FutureBuilder(
        future: KategoriBarangApi().getKategoriBarang(1, 1, ""),
        builder: (context, s) {
          if (s.data == null) {
            return Center(
              child: ListSkaleton(),
            );
          } else {
            return CategoryList(
              kategoriBarangModel: s.data,
            );
          }
        },
      ),
    );
  }

  static Future<void> addCategory(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          title: Row(
            children: [
              Icon(
                Icons.create,
                color: Pallet.primaryColor,
              ),
              Text(
                ' Tambah Kategori',
                style: Style.fontMediumBold,
              )
            ],
          ),
          content: Text(
            'Apakah anda yakin akan menghapush item ini?',
            style: TextStyle(
              color: Colors.black54,
            ),
          ),
          actions: [
            RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              // color: Colors.grey,
              child: Text('Batal'),
            ),
            RaisedButton(
              onPressed: () {},
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              color: Colors.indigo,
              child: Text('Ok'),
            ),
          ],
        );
      },
    );
  }
}

class CategoryList extends StatefulWidget {
  final KategoriBarangModel kategoriBarangModel;
  const CategoryList({
    this.kategoriBarangModel,
    Key key,
  }) : super(key: key);

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  ScrollController scrollController = new ScrollController();
  List<KategoriBarangDataModel> data;
  int currentPage;
  
  @override
  void initState() {
    data = widget.kategoriBarangModel.data;
    currentPage = 1;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, i) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 5.0),
                title: Text(
                  data[i].nama_kategori_barang,
                  style: Style.fontMediumBlackBold,
                ),
                subtitle: Text(
                  "${i} Menu",
                  style: Style.fontSmallBold,
                ),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              Divider(
                color: Colors.grey,
                height: 1.0,
              )
            ],
          ),
        );
      },
    );
  }
}
