import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mama_mitra/config/palette.dart';

class formWidget {
  InputDecoration _inputDecoration(label) {
    return InputDecoration(
      contentPadding: EdgeInsets.all(10.0),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: BorderSide(color: Pallet.primaryColor, width: 1.0),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: BorderSide(color: Colors.black45, width: 1.0),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: BorderSide(color: Colors.black45, width: 1.0),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: BorderSide(color: Colors.red, width: 1.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: BorderSide(color: Pallet.primaryColor, width: 1.0),
      ),
      labelText: label,
      labelStyle: Style.fontMediumBold,
    );
  }

  Widget textFormFieldIDR(TextEditingController controller, String label,
      TextInputType type, int maxLength) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5.0),
      child: TextFormField(
        keyboardType: type,
        controller: controller,
        inputFormatters: [
          WhitelistingTextInputFormatter.digitsOnly,
          CurrencyPtBrInputFormatter(),
        ],
        decoration: _inputDecoration(label),
        maxLength: maxLength,
        validator: (String value) {
          if (value.isEmpty) {
            return label + ' Harus Diisi';
          }
          return null;
        },
      ),
    );
  }

  Widget textFormField(
    TextEditingController controller,
    String label,
    TextInputType type,
    int maxLength,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5.0),
      child: TextFormField(
        textCapitalization: TextCapitalization.sentences,
        keyboardType: type,
        controller: controller,
        decoration: _inputDecoration(label),
        maxLength: maxLength,
        validator: (String value) {
          if (value.isEmpty) {
            return label + ' Harus Diisi';
          }
          return null;
        },
      ),
    );
  }

  Widget textFormAreaField(TextEditingController controller, String label,
      TextInputType type, int maxLength) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5.0),
      child: TextFormField(
        maxLines: 4,
        keyboardType: type,
        controller: controller,
        decoration: _inputDecoration(label),
        maxLength: maxLength,
        validator: (String value) {
          if (value.isEmpty) {
            return label + ' Harus Diisi';
          }
          return null;
        },
      ),
    );
  }

  Widget textFormFieldClickable(TextEditingController controller, String label,
      TextInputType type, int maxLength, IconData icon, f) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5.0),
      child: InkWell(
        onTap: f,
        child: TextFormField(
          enabled: false,
          keyboardType: type,
          controller: controller,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(10.0),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Pallet.primaryColor, width: 1.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Colors.black45, width: 1.0),
            ),
            disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Colors.black45, width: 1.0),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Colors.red, width: 1.0),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Pallet.primaryColor, width: 1.0),
            ),
            labelText: label,
            labelStyle: Style.fontMediumBold,
            suffixIcon: Icon(icon),
          ),
          maxLength: maxLength,
          validator: (String value) {
            if (value.isEmpty) {
              return label + ' Harus Diisi';
            }
            return null;
          },
        ),
      ),
    );
  }
}

class CurrencyPtBrInputFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }
    double value = double.parse(newValue.text);
    String newText =
        NumberFormat.simpleCurrency(locale: "IDR", decimalDigits: 0)
            .format(value);
    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}
