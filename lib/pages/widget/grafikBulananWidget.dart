import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:mama_mitra/api/grafikApi.dart';
import 'package:mama_mitra/config/palette.dart';
import 'package:mama_mitra/models/grafikBulananModel.dart';

class GrafikBulananWidget extends StatefulWidget {
  @override
  _GrafikBulananWidgetState createState() => _GrafikBulananWidgetState();
}

class _GrafikBulananWidgetState extends State<GrafikBulananWidget> {
  bool is_load_data = true;
  List<charts.Series<dynamic, String>> dataGrafikBulanan;

  void getDataGafikBulanan() {
    setState(() {
      is_load_data = true;
    });
    GrafikApi().getGrafikBulanan().then((r) {
      GrafikBulananModel grafikBulananModel = r;
      setState(() {
        dataGrafikBulanan = [
          new charts.Series<GrafikBulananDataModel, String>(
            id: "Penjualan",
            colorFn: (_, __) =>
                charts.ColorUtil.fromDartColor(Colors.pink[400]),
            domainFn: (GrafikBulananDataModel grafikBulananDataModel, _) =>
                grafikBulananDataModel.nama_bulan,
            measureFn: (GrafikBulananDataModel grafikBulananDataModel, _) =>
                double.parse(grafikBulananDataModel.jumlah_penjualan),
            data: grafikBulananModel.data,
          ),
        ];
        is_load_data = false;
      });
    }).catchError((e) {
      print("gagal memuat data");
      setState(() {
        is_load_data = false;
      });
    });
  }

  @override
  void initState() {
    getDataGafikBulanan();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: is_load_data == true
          ? Container(
              height: 300,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                height: 300,
                width: 500,
                child: charts.BarChart(
                  dataGrafikBulanan,
                  animate: true,
                ),
              ),
            ),
    );
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
