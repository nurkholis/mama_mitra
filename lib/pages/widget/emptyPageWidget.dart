import 'package:flutter/material.dart';
import 'package:mama_mitra/config/palette.dart';

class EmptyPageWidget {
  Widget noInternetConnection(f) {
    return template(
        "assets/images/illustration/login.png",
        "Opps! Tidak Ada Koneksi Internet",
        "Koneksi internet tidak ditemukan, pastikan perangkat anda memiliki koneksi internet atau coba lagi.",
        f);
  }

  Widget noData(f) {
    return template(
        "assets/images/illustration/login.png",
        "Opps! Tidak Ada Data",
        "Tidak ada data untuk ditampilkan. anda dapat menambah data dengan menekan tombol tambah.",
        f);
  }

  Widget noSearchFound() {
    return template(
        "assets/images/illustration/login.png",
        "Opps! Tidak Ada Koneksi Internet",
        "Koneksi internet tidak ditemukan, pastikan perangkat anda memiliki koneksi internet atau coba lagi.",
        null);
  }

  Widget template(String image, String title, String subTitle, f) {
    return Container(
      child: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 40.0, horizontal: 10.0),
                child: Image.asset(
                  image,
                  width: 200.0,
                  height: 200.0,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  title,
                  style: Style.fontLargeBlackBold,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
                child: Text(
                  subTitle,
                  style: Style.fontSmall,
                  textAlign: TextAlign.center,
                ),
              ),
              f != null
                  ? Padding(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: RaisedButton(
                        onPressed: f,
                        elevation: 0.0,
                        padding: EdgeInsets.all(15.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: Pallet.primaryColor),
                        ),
                        color: Colors.white,
                        child: Text(
                          "Coba lagi",
                          style: Style.fontMediumPrimary,
                        ),
                      ),
                    )
                  : null,
            ],
          ),
        ),
      ),
    );
  }
}
