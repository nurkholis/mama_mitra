import 'package:flutter/material.dart';
import 'package:mama_mitra/config/palette.dart';

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new WillPopScope(
          onWillPop: () async => false,
          child: SimpleDialog(
            key: key,
            // backgroundColor: Colors.black54,
            children: <Widget>[
              Center(
                child: Column(
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Mohon menunggu...",
                      style: TextStyle(color: Colors.blueAccent),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  static Future<void> showAlertSnackbar(BuildContext context,
      GlobalKey<ScaffoldState> key, String title, String subtitle) async {
    key.currentState.showSnackBar(
      SnackBar(
        content: Container(
          padding: EdgeInsets.all(5.0),
          child: Wrap(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    Icons.info_outline,
                    color: Colors.white,
                    size: 30.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      title,
                      style: Style.fontMediumWhite,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(subtitle),
              ),
            ],
          ),
        ),
        duration: Duration(seconds: 3),
        backgroundColor: Colors.red,
      ),
    );
  }

  static Future<void> showInfoSnackbar(BuildContext context,
      GlobalKey<ScaffoldState> key, String title, String subtitle) async {
    key.currentState.showSnackBar(
      SnackBar(
        content: Container(
          padding: EdgeInsets.all(5.0),
          child: Wrap(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    Icons.check_circle_outline,
                    color: Colors.white,
                    size: 30.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      title,
                      style: Style.fontMediumWhite,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(subtitle),
              ),
            ],
          ),
        ),
        duration: Duration(seconds: 3),
        backgroundColor: Colors.green,
      ),
    );
  }

  static Future<void> showConfirmDialog(BuildContext context, GlobalKey key,
      String subtitle, void action()) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          title: Row(children: [
            Icon(
              Icons.info_outline,
              color: Colors.orangeAccent,
            ),
            Text(
              ' Konfirmasi',
              style: TextStyle(color: Colors.black54),
            )
          ]),
          content: Text(
            subtitle,
            style: TextStyle(
              color: Colors.black54,
            ),
          ),
          actions: [
            RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              elevation: 0.0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(color: Colors.grey),
              ),
              // color: Colors.grey,
              child: Text(
                'Batal',
                style: Style.fontMediumBold,
              ),
            ),
            RaisedButton(
              onPressed: action,
              elevation: 0.0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(color: Pallet.primaryColor),
              ),
              child: Text(
                'Ok',
                style: Style.fontMediumPrimary,
              ),
            ),
          ],
        );
      },
    );
  }
}
