import 'package:flutter/material.dart';

class Pallet {
  static const Color primaryColor = Color(0xFF473F97);
}

class Style {
  static const fontSmall = TextStyle(
    fontSize: 12.0,
    color: Colors.black54,
    fontWeight: FontWeight.w400
  );
  static const fontSmallWhite = TextStyle(
    fontSize: 12.0,
    color: Colors.white,
    fontWeight: FontWeight.w600,
  );
  static const fontSmallPrimary = TextStyle(
    fontSize: 12.0,
    color: Pallet.primaryColor,
    fontWeight: FontWeight.w600,
  );
  static const fontSmallBold = TextStyle(
    fontSize: 12.0,
    color: Colors.black54,
    fontWeight: FontWeight.w600,
  );
  static const fontMedium = TextStyle(
    fontSize: 16.0,
    color: Colors.black87,
    
  );
  static const fontMediumWhite = TextStyle(
    fontSize: 16.0,
    color: Colors.white,
    fontWeight: FontWeight.w600,
  );
  static const fontMediumPrimary = TextStyle(
    fontSize: 16.0,
    color: Pallet.primaryColor,
    fontWeight: FontWeight.w600,
  );
  static const fontMediumBold = TextStyle(
    fontSize: 16.0,
    color: Colors.black54,
    fontWeight: FontWeight.w600,
  );
  static const fontMediumBlackBold = TextStyle(
    fontSize: 16.0,
    color: Colors.black87,
    fontWeight: FontWeight.w600,
  );
  static const fontLarge = TextStyle(
    fontSize: 20.0,
    color: Colors.black87,
  );
  static const fontLargeWhite = TextStyle(
    fontSize: 20.0,
    color: Colors.white,
    fontWeight: FontWeight.w600,
  );
  static const fontLargePrimary = TextStyle(
    fontSize: 20.0,
    color: Pallet.primaryColor,
    fontWeight: FontWeight.w600,
  );
  static const fontLargeBold = TextStyle(
    fontSize: 20.0,
    color: Colors.black54,
    fontWeight: FontWeight.w600,
  );
  static const fontLargeBlackBold = TextStyle(
    fontSize: 20.0,
    color: Colors.black87,
    fontWeight: FontWeight.w600,
  );
}
