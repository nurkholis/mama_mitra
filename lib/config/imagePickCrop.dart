import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:mama_mitra/config/palette.dart';

class ImagePickCrop {
  static AndroidUiSettings _androidSetting = AndroidUiSettings(
      toolbarTitle: 'Edit Gmbar',
      toolbarColor: Pallet.primaryColor,
      toolbarWidgetColor: Colors.white,
      initAspectRatio: CropAspectRatioPreset.square,
      hideBottomControls: true,
      lockAspectRatio: true);

  static Future<File> showChoiceDialog(BuildContext context) async {
    File result;
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Pilih sumber gambar",
            style: Style.fontMediumBlackBold,
          ),
          content: SingleChildScrollView(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  onPressed: () async {
                    File a = await ImagePicker.pickImage(
                      source: ImageSource.gallery,
                    );
                    if (a != null) {
                      result = await ImageCropper.cropImage(
                        sourcePath: a.path,
                        androidUiSettings: _androidSetting,
                        iosUiSettings: IOSUiSettings(
                          title: 'Edit Gmbar',
                        ),
                      );
                    }
                    Navigator.of(context).pop();
                  },
                  elevation: 0.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(color: Pallet.primaryColor),
                  ),
                  color: Colors.white,
                  child: Text(
                    "Galeri",
                    style: Style.fontMediumPrimary,
                  ),
                ),
                RaisedButton(
                  onPressed: () async {
                    File a = await ImagePicker.pickImage(
                      source: ImageSource.camera,
                    );
                    if (a != null) {
                      result = await ImageCropper.cropImage(
                        sourcePath: a.path,
                        androidUiSettings: _androidSetting,
                        iosUiSettings: IOSUiSettings(
                          title: 'Edit Gmbar',
                        ),
                      );
                    }
                    Navigator.of(context).pop();
                  },
                  elevation: 0.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(color: Pallet.primaryColor),
                  ),
                  color: Colors.white,
                  child: Text(
                    "Kamera",
                    style: Style.fontMediumPrimary,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
    return result;
  }
}
