class EndPoint {
  // static final host = 'http://mama.kodejariah.com';
  static final host = 'http://192.168.43.60:8000';
  static final name = {
    'member': host + '/api/member',
    'toko': host + '/api/toko',
    'toko.byid': host + '/api/toko/byid',
    'toko.login': host + '/api/toko/login',
    'kategori.barang': host + '/api/kategori/barang',
    'barang': host + '/api/barang',
    'penjualan': host + '/api/penjualan',
    'grafik.bulanan': host + '/api/grafik/bulanan',
  };
}
