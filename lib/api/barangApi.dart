import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mama_mitra/models/barangModel.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:mama_mitra/models/response_error_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BarangApi {
  Future<BarangModel> getBarang(int page, int limit, String param) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final response = await http.get(EndPoint.name['barang'] +
          "?page=${page.toString()}&limit=${limit.toString()}&id_toko=${prefs.getString('id_toko')}");
      if (response.statusCode == 200) {
        return BarangModel.fromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<String> storeBarang(BarangDataModel data, String _method) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    String endPoint;
    _method == 'POST'
        ? endPoint = EndPoint.name['barang']
        : endPoint = EndPoint.name['barang'] + '/' + data.id_barang;
    final response = await http.post(endPoint, body: {
      'id_kategori_barang': data.id_kategori_barang,
      'nama_barang': data.nama_barang,
      'harga_barang': data.harga_barang,
      'harga_promo': data.harga_promo,
      'deskripsi_barang': data.deskripsi_barang,
      'foto_barang': data.foto_barang,
      'terlihat': data.terlihat,
      '_method': _method
    }, headers: {
      'Authorization': 'Bearer ' + token,
      'Accept': 'application/json'
    });
    if (response.statusCode == 201 || response.statusCode == 200) {
      return 'success';
    }
    final error = ResponseErrorModel.fromJson(json.decode(response.body));
    return error.errors;
  }
}
