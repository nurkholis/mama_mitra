import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mama_mitra/models/penjualanModel.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PenjualanApi {
  Future<PenjualanModel> getPenjualan(int page, int limit, String param) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final response = await http.get(EndPoint.name['penjualan'] +
          "?page=${page.toString()}&limit=${limit.toString()}&id_toko=${prefs.getString('id_toko')}");
      if (response.statusCode == 200) {
        return PenjualanModel.fromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
