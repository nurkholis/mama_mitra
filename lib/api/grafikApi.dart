import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mama_mitra/config/endPoint.dart';
import 'package:mama_mitra/models/grafikBulananModel.dart';
import 'package:mama_mitra/models/response_error_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GrafikApi {
  Future<GrafikBulananModel> getGrafikBulanan() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String tahun = "2020";
      String id_toko = prefs.getString("id_toko");
      final response = await http
          .get("${EndPoint.name['grafik.bulanan']}/${id_toko}/${tahun}");
      if (response.statusCode == 200) {
        return GrafikBulananModel.fromJson(json.decode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
