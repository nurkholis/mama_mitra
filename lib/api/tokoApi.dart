import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mama_mitra/models/response_error_model.dart';
import 'package:mama_mitra/models/tokoModel.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokoApi {
  Future<TokoModel> getToko(int page, int limit, String param) async {
    final response = await http.get(EndPoint.name['toko']);
    if (response.statusCode == 200) {
      return TokoModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Faild to load');
    }
  }

  Future<TokoDataModel> getTokoku() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = await http
        .get(EndPoint.name['toko.byid'] + "/" + prefs.getString("id_toko"));
    if (response.statusCode == 200) {
      return TokoDataModel.fromJson(json.decode(response.body)['data']);
    } else {
      throw Exception('Faild to load');
    }
  }

  Future<bool> loginToko(
      String email, String password, String device_id) async {
    final response = await http.post(EndPoint.name['toko.login'], body: {
      "email": email,
      "password": password,
      "device_id": device_id,
    });
    print(response.body);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', result['meta']['token'].toString());
      prefs.setString('id_toko', result['data']['id_toko'].toString());
      return true;
    }
    return false;
  }

  Future<String> storeToko(TokoDataModel data, String _method) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    String endPoint;
    _method == 'POST'
        ? endPoint = EndPoint.name['toko']
        : endPoint = EndPoint.name['toko'] + '/' + data.id_toko;
    print(_method);
    final response = await http.post(endPoint, body: {
      'nama_toko': data.nama_toko,
      'telepon_toko': data.telepon_toko,
      'logo_toko': data.logo_toko,
      '_method': _method
    }, headers: {
      'Authorization': 'Bearer ' + token,
      'Accept': 'application/json'
    });
    if (response.statusCode == 201 || response.statusCode == 200) {
      return 'success';
    }
    final error = ResponseErrorModel.fromJson(json.decode(response.body));
    return error.errors;
  }

  Future<String> gantiPassword(String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    String endPoint = EndPoint.name['toko'] + '/' + prefs.getString("id_toko");
    final response = await http.post(endPoint, body: {
      'password': password,
      '_method': "PUT"
    }, headers: {
      'Authorization': 'Bearer ' + token,
      'Accept': 'application/json'
    });
    if (response.statusCode == 201 || response.statusCode == 200) {
      return 'success';
    }
    final error = ResponseErrorModel.fromJson(json.decode(response.body));
    return error.errors;
  }
}
