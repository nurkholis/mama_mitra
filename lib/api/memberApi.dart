import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mama_mitra/models/memberModel.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MemberApi {

  Future<MemberModel> getMember(int page, int limit, String param) async {
    final response = await http.get(EndPoint.name['member']);
    if (response.statusCode == 200) {
      return MemberModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Faild to load');
    }
  }

  Future<bool> loginMemer(String email, String password, String device_id) async{
    final response = await http.post(EndPoint.name['member.login'],
      body: {
        "email" : email,
        "password" : password,
        "device_id" : device_id,
      }
    );
    if(response.statusCode == 200){
      final result = json.decode(response.body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', result['meta']['token'].toString());
      prefs.setString('id_member', result['data']['id_member'].toString());
      return true;
    }
    return false;
  }

}