import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mama_mitra/models/kategoriBarangModel.dart';
import 'package:mama_mitra/config/endPoint.dart';
import 'package:mama_mitra/models/response_error_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class KategoriBarangApi {
  Future<KategoriBarangModel> getKategoriBarang(
      int page, int limit, String param) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = await http.get(EndPoint.name['kategori.barang'] +
        "?page=${page.toString()}&limit=${limit.toString()}&id_toko=${prefs.getString('id_toko')}");
    if (response.statusCode == 200) {
      return KategoriBarangModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Faild to load');
    }
  }

  Future<KategoriBarangDataModel> storeKategoriBarang(
      KategoriBarangDataModel data, String _method) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    String endPoint;
    _method == 'POST'
        ? endPoint = EndPoint.name['kategori.barang']
        : endPoint =
            EndPoint.name['kategori.barang'] + '/' + data.id_kategori_barang;
    final response = await http.post(endPoint, body: {
      'nama_kategori_barang': data.nama_kategori_barang,
      '_method': _method
    }, headers: {
      'Authorization': 'Bearer ' + token,
      'Accept': 'application/json'
    });
    if (response.statusCode == 201 || response.statusCode == 200) {
      var res = json.decode(response.body);
      return KategoriBarangDataModel.fromJson(res['data']);
    }
  }
}
