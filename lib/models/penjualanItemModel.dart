import 'package:mama_mitra/models/barangModel.dart';

class PenjualanItemModel {
  List<PenjualanItemDataModel> data;
  PenjualanItemPaginationModel pagination;

  PenjualanItemModel({
    this.data,
    this.pagination,
  });

  factory PenjualanItemModel.fromJson(Map<String, dynamic> json) {
    return PenjualanItemModel(
      data: List<PenjualanItemDataModel>.from(json['data'].map((x) {
        return PenjualanItemDataModel.fromJson(x);
      })),
      pagination: json.containsKey("meta")
          ? PenjualanItemPaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class PenjualanItemDataModel {
  String id_penjualan_item;
  String jumlah_barang;
  String harga_jual;
  String subtotal_penjualan;
  BarangDataModel barang;

  PenjualanItemDataModel({
    this.id_penjualan_item,
    this.jumlah_barang,
    this.harga_jual,
    this.subtotal_penjualan,
    this.barang,
  });

  factory PenjualanItemDataModel.fromJson(Map<String, dynamic> json) {
    return PenjualanItemDataModel(
      id_penjualan_item: json["id_penjualan_item"].toString(),
      jumlah_barang: json["jumlah_barang"].toString(),
      harga_jual: json["harga_jual"].toString(),
      subtotal_penjualan: json["subtotal_penjualan"].toString(),
      barang: json.containsKey("barang") ? BarangDataModel.fromJson(json['barang']['data']) : null,
    );
  }
}

class PenjualanItemPaginationModel {
  int current_page;
  int total_pages;
  int per_page;
  int total;

  PenjualanItemPaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory PenjualanItemPaginationModel.fromJson(Map<String, dynamic> json) {
    return PenjualanItemPaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}
