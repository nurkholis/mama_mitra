class MemberModel {
  List<MemberDataModel> data;
  MemberPaginationModel pagination;

  MemberModel({
    this.data,
    this.pagination,
  });

  factory MemberModel.fromJson(Map<String, dynamic> json) {
    return MemberModel(
      data: List<MemberDataModel>.from(json['data'].map((x) {
        return MemberDataModel.fromJson(x);
      })),
      pagination: json.containsKey("meta")
          ? MemberPaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class MemberDataModel {
  String id_member;
  String nama_member;

  MemberDataModel({
    this.id_member,
    this.nama_member,
  });

  factory MemberDataModel.fromJson(Map<String, dynamic> json) {
    return MemberDataModel(
      id_member: json["id_member"].toString(),
      nama_member: json["nama_member"].toString(),
    );
  }
}

class MemberPaginationModel {
  int current_page;
  int total_pages;
  int per_page;
  int total;

  MemberPaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory MemberPaginationModel.fromJson(Map<String, dynamic> json) {
    return MemberPaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}
