class DriverModel {
  List<DriverDataModel> data;
  DriverPaginationModel pagination;

  DriverModel({
    this.data,
    this.pagination,
  });

  factory DriverModel.fromJson(Map<String, dynamic> json) {
    return DriverModel(
      data: List<DriverDataModel>.from(json['data'].map((x) {
        return DriverDataModel.fromJson(x);
      })),
      pagination: json.containsKey("meta")
          ? DriverPaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class DriverDataModel {
  String id_driver;
  String nama_driver;
  String email;
  String telepon_driver;
  String foto_driver;
  String terdaftar;

  DriverDataModel({
    this.id_driver,
    this.nama_driver,
    this.email,
    this.telepon_driver,
    this.foto_driver,
    this.terdaftar,
  });

  factory DriverDataModel.fromJson(Map<String, dynamic> json) {
    return DriverDataModel(
      id_driver: json["id_driver"].toString(),
      nama_driver: json["nama_driver"].toString(),
      email: json["email"].toString(),
      telepon_driver: json["telepon_driver"].toString(),
      foto_driver: json["foto_driver"].toString(),
      terdaftar: json["terdaftar"].toString(),
    );
  }
}

class DriverPaginationModel {
  int current_page;
  int total_pages;
  int per_page;
  int total;

  DriverPaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory DriverPaginationModel.fromJson(Map<String, dynamic> json) {
    return DriverPaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}
