class TokoModel {
  List<TokoDataModel> data;
  TokoPaginationModel pagination;

  TokoModel({
    this.data,
    this.pagination,
  });

  factory TokoModel.fromJson(Map<String, dynamic> json) {
    return TokoModel(
      data: List<TokoDataModel>.from(json['data'].map((x) {
        return TokoDataModel.fromJson(x);
      })),
      pagination: json.containsKey("meta")
          ? TokoPaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class TokoDataModel {
  String id_toko;
  String nama_toko;
  String email;
  String telepon_toko;
  String logo_toko;
  String alamat_lengkap_toko;
  String alamat_gps_toko;

  TokoDataModel({
    this.id_toko,
    this.nama_toko,
    this.email,
    this.telepon_toko,
    this.logo_toko,
    this.alamat_lengkap_toko,
    this.alamat_gps_toko,
  });

  factory TokoDataModel.fromJson(Map<String, dynamic> json) {
    return TokoDataModel(
      id_toko: json["id_toko"].toString(),
      nama_toko: json["nama_toko"].toString(),
      email: json["email"].toString(),
      telepon_toko: json["telepon_toko"].toString(),
      logo_toko: json["logo_toko"].toString(),
      alamat_lengkap_toko: json["alamat_lengkap_toko"].toString(),
      alamat_gps_toko: json["foto_toko"].toString(),
    );
  }
}

class TokoPaginationModel {
  int current_page;
  int total_pages;
  int per_page;
  int total;

  TokoPaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory TokoPaginationModel.fromJson(Map<String, dynamic> json) {
    return TokoPaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}
