import 'package:mama_mitra/models/driverModel.dart';
import 'package:mama_mitra/models/memberModel.dart';
import 'package:mama_mitra/models/penjualanItemModel.dart';
import 'package:mama_mitra/models/tokoModel.dart';
import 'kategoriBarangModel.dart';

class PenjualanModel {
  List<PenjualanDataModel> data;
  PenjualanPaginationModel pagination;

  PenjualanModel({
    this.data,
    this.pagination,
  });

  factory PenjualanModel.fromJson(Map<String, dynamic> json) {
    return PenjualanModel(
      data: List<PenjualanDataModel>.from(json['data'].map((x) {
        return PenjualanDataModel.fromJson(x);
      })),
      pagination: json.containsKey("meta")
          ? PenjualanPaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class PenjualanDataModel {
  String id_penjualan;
  String kode_penjualan;
  String total_penjualan;
  String status_penjualan;
  String ongkir;
  String alamat_lengkap_penjualan;
  String alamat_gps_penjualan;
  String jam;
  String tanggal;
  String waktu;
  MemberDataModel member;
  TokoDataModel toko;
  DriverDataModel driver;
  PenjualanItemModel penjualanItem;

  PenjualanDataModel({
    this.id_penjualan,
    this.kode_penjualan,
    this.total_penjualan,
    this.ongkir,
    this.status_penjualan,
    this.alamat_lengkap_penjualan,
    this.alamat_gps_penjualan,
    this.jam,
    this.tanggal,
    this.waktu,
    this.member,
    this.toko,
    this.driver,
    this.penjualanItem,
  });

  factory PenjualanDataModel.fromJson(Map<String, dynamic> json) {
    return PenjualanDataModel(
      id_penjualan: json["id_penjualan"].toString(),
      kode_penjualan: json["kode_penjualan"].toString(),
      total_penjualan: json["total_penjualan"].toString(),
      status_penjualan: json["status_penjualan"].toString(),
      ongkir: json["ongkir"].toString(),
      alamat_lengkap_penjualan: json["alamat_lengkap_penjualan"].toString(),
      alamat_gps_penjualan: json["alamat_gps_penjualan"].toString(),
      jam: json["jam"].toString(),
      tanggal: json["tanggal"].toString(),
      waktu: json["waktu"].toString(),
      member: json.containsKey("member") ? MemberDataModel.fromJson(json['member']['data']) : null,
      toko: json.containsKey("toko") ? TokoDataModel.fromJson(json['toko']['data']) : null,
      driver: json.containsKey("driver") ? DriverDataModel.fromJson(json['driver']['data']) : null,
      penjualanItem: json.containsKey("penjualanItem") ? PenjualanItemModel.fromJson(json['penjualanItem']) : null,
    );
  }
}

class PenjualanPaginationModel {
  int current_page;
  int total_pages;
  int per_page;
  int total;

  PenjualanPaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory PenjualanPaginationModel.fromJson(Map<String, dynamic> json) {
    return PenjualanPaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}
