class GrafikBulananModel {
  List<GrafikBulananDataModel> data;
  GrafikBulananPaginationModel pagination;
  String token;

  GrafikBulananModel({
    this.data,
    this.pagination,
    this.token,
  });

  factory GrafikBulananModel.fromJson(Map<String, dynamic> json) {
    return GrafikBulananModel(
      data: List<GrafikBulananDataModel>.from(json['data'].map((x) {
        return GrafikBulananDataModel.fromJson(x);
      })),
      pagination: json.containsKey('meta')
          ? GrafikBulananPaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class GrafikBulananDataModel {
  String nomor_bulan;
  String nama_bulan;
  String jumlah_penjualan;

  GrafikBulananDataModel({
    this.nomor_bulan,
    this.nama_bulan,
    this.jumlah_penjualan,
  });

  factory GrafikBulananDataModel.fromJson(Map<String, dynamic> json) {
    return GrafikBulananDataModel(
      nomor_bulan: json["nomor_bulan"].toString(),
      nama_bulan: json["nama_bulan"].toString(),
      jumlah_penjualan: json["jumlah_penjualan"].toString(),
    );
  }
}

class GrafikBulananPaginationModel {
  int current_page;
  int total_pages;
  int per_page;
  int total;

  GrafikBulananPaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory GrafikBulananPaginationModel.fromJson(Map<String, dynamic> json) {
    return GrafikBulananPaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}
