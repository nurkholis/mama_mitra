import 'dart:convert';

class ResponseErrorModel {
  String messege;
  String errors;

  ResponseErrorModel({
    this.messege,
    this.errors
  });

  factory ResponseErrorModel.fromJson(Map<String, dynamic> json){
    final parsedJson = json['errors'];
    String result = '';
    parsedJson.forEach((key, value){
      for (var item in value) {
        result = result +  item + '\n';
      }
    });
    return ResponseErrorModel(
      messege: json["message"],
      errors: result,
    );
  }
}
