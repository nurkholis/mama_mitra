import 'kategoriBarangModel.dart';

class BarangModel {
  List<BarangDataModel> data;
  BarangPaginationModel pagination;
  String token;

  BarangModel({
    this.data,
    this.pagination,
    this.token,
  });

  factory BarangModel.fromJson(Map<String, dynamic> json) {
    return BarangModel(
      data: List<BarangDataModel>.from(json['data'].map((x) {
        return BarangDataModel.fromJson(x);
      })),
      pagination: json.containsKey("meta")
          ? BarangPaginationModel.fromJson(json['meta']['pagination'])
          : null,
    );
  }
}

class BarangDataModel {
  String id_barang;
  String nama_barang;
  String id_toko;
  String id_kategori_barang;
  String harga_barang;
  String harga_promo;
  String berat_barang;
  String foto_barang;
  String stok_barang;
  String deskripsi_barang;
  String terlihat;
  KategoriBarangDataModel kategori_barang;

  BarangDataModel({
    this.id_barang,
    this.nama_barang,
    this.id_toko,
    this.id_kategori_barang,
    this.harga_barang,
    this.harga_promo,
    this.berat_barang,
    this.foto_barang,
    this.stok_barang,
    this.deskripsi_barang,
    this.kategori_barang,
    this.terlihat,
  });

  factory BarangDataModel.fromJson(Map<String, dynamic> json) {
    return BarangDataModel(
      id_barang: json["id_barang"].toString(),
      id_toko: json["id_toko"].toString(),
      id_kategori_barang: json["id_kategori_barang"].toString(),
      nama_barang: json["nama_barang"].toString(),
      harga_barang: json["harga_barang"].toString(),
      harga_promo: json["harga_promo"].toString(),
      berat_barang: json["berat_barang"].toString(),
      foto_barang: json["foto_barang"].toString(),
      stok_barang: json["stok_barang"].toString(),
      deskripsi_barang: json["deskripsi_barang"].toString(),
      terlihat: json["terlihat"].toString(),
      kategori_barang: json.containsKey("kategoriBarang")
          ? KategoriBarangDataModel.fromJson(json['kategoriBarang']['data'])
          : null,
    );
  }
}

class BarangPaginationModel {
  int current_page;
  int total_pages;
  int per_page;
  int total;

  BarangPaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory BarangPaginationModel.fromJson(Map<String, dynamic> json) {
    return BarangPaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}
