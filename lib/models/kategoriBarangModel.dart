class KategoriBarangModel {
  List<KategoriBarangDataModel> data;
  KategoriBarangPaginationModel pagination;
  String token;

  KategoriBarangModel({
    this.data,
    this.pagination,
    this.token,
  });

  factory KategoriBarangModel.fromJson(Map<String, dynamic> json) {
    return KategoriBarangModel(
      data: List<KategoriBarangDataModel>.from(json['data'].map((x) {
        return KategoriBarangDataModel.fromJson(x);
      })),
      pagination: json.containsKey('meta') ? KategoriBarangPaginationModel.fromJson(json['meta']['pagination']) : null,
    );
  }
}

class KategoriBarangDataModel {
  String id_kategori_barang;
  String nama_kategori_barang;

  KategoriBarangDataModel({
    this.id_kategori_barang,
    this.nama_kategori_barang,
  });

  factory KategoriBarangDataModel.fromJson(Map<String, dynamic> json) {
    return KategoriBarangDataModel(
      id_kategori_barang: json["id_kategori_barang"].toString(),
      nama_kategori_barang: json["nama_kategori_barang"],
    );
  }
}

class KategoriBarangPaginationModel{
  int current_page;
  int total_pages;
  int per_page;
  int total;

  KategoriBarangPaginationModel({
    this.current_page,
    this.total_pages,
    this.per_page,
    this.total,
  });

  factory KategoriBarangPaginationModel.fromJson(Map<String, dynamic> json) {
    return KategoriBarangPaginationModel(
      current_page: json['current_page'],
      total_pages: json['total_pages'],
      per_page: json['per_page'],
      total: json['total'],
    );
  }
}